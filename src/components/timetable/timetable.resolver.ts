import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import { AngularDataContext } from '@themost/angular';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

export class TimetableTableConfigurationResolver implements Resolve<String> {
    constructor(private httpClient: HttpClient) { }
    
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<String> {

        const configSrc = `assets/lists/TimetableEvents/${route.params.list}.json`;
        const defaultConfigSrc = `assets/lists/TimetableEvents/all.json`;
        return this.httpClient.get(configSrc).pipe(map(response => configSrc), catchError(error => of(defaultConfigSrc)));
    }
}

export class TimetableTableSearchResolver implements Resolve<String> {
    constructor(private httpClient: HttpClient) { }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<String> {
        const configSrc = `assets/lists/TimetableEvents/${route.params.list}.search.json`;
        const defaultConfigSrc = `assets/lists/TimetableEvents/search.json`;
        return this.httpClient.get(configSrc).pipe(map(response => configSrc), catchError(error => of(defaultConfigSrc)));
    }
}

export class CourseClassInstructorResolver implements Resolve<any> {
    constructor(private _context: AngularDataContext) {}

    resolve(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Promise<any> {
        return this._context.model('CourseClassInstructors')
            .where('id')
            .equal(route.params.id)
            .expand('courseClass($expand=course),instructor')
            .getItem();
    }
}
