import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { AdvancedFormItemResolver } from '@universis/forms';
import { AngularDataContext } from '@themost/angular';

@Injectable()
export class EventResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext,
              private _itemResolver: AdvancedFormItemResolver) { }
    async resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
      ): Promise<Promise<any> | any> {

        const timetableDateRange = await this._context.model('TimetableEvents')
            .where('id')
            .equal(route.params.timetable)
            .select('startDate, endDate')
            .getItem();

        return this._itemResolver.resolve(route, state).then((item: any) => {
          Object.assign(item, {
            willBeScheduled: (item.startDate == null && item.endDate == null)
          });
          if (timetableDateRange) {
            Object.assign(item, {
              timetableStartDate: timetableDateRange.startDate,
              timetableEndDate: timetableDateRange.endDate
            });
          }
          if (item.duration) {
            Object.assign(item, {
              totalDuration: moment.duration(item.duration).asMinutes()
            });
          }
          // todo: remove startTime and endTime attributes
          if (item.eventHoursSpecification == null) {
            if (item.startDate) {
              Object.assign(item, {
                startTime: item.startDate
              });
            }
            if (item.endDate) {
              Object.assign(item, {
                endTime: item.endDate
              });
            }
          }
          return item;
        });
      }
}