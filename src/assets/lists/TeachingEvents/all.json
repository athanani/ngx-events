{
	"title": "Events.All",
	"model": "TeachingEvents",
	"searchExpression": "(indexof(name, '${text}') ge 0) or (day(startDate) eq ${/^\\d+$/.test(text) ? text : 0}) or (indexof(description, '${text}') ge 0) or (indexof(courseClass/course/displayCode, '${text}') ge 0)",
	"calendarEventTitleTemplate": "${startDate.toTimeString().substring(0,5)} - ${endDate.toTimeString().substring(0,5)} - ${courseClassDisplayCode} - ${name} - ${performer}<% if (location) { %> - ${location} <% } %>",
	"selectable": false,
	"defaults": {
		"orderBy": "courseClass/title, startDate desc",
		"filter": "((superEvent eq ${timetable} and eventHoursSpecification eq null) or (superEvent/superEvent eq ${timetable}))",
		"expand": "sections"
	},
	"multipleSelect": true,
	"columns": [
		{
			"name": "id",
			"property": "id",
			"hidden": true
		},
		{
			"name": "startDate",
			"property": "startDate",
			"formatter": "DateFormatter",
			"label": "Date",
			"title": "Events.Date",
			"className": "text-nowrap"
		},
		{
			"name": "endDate",
			"property": "endDate",
			"hidden": true
		},
		{
			"name": "name",
			"property": "name",
			"title": "Events.Title",
			"label": "Name",
			"hidden": false
		},
		{
			"name": "location/displayCode",
			"property": "placeCode",
			"title": "Events.Places.Code"
		},
		{
			"name": "location/name",
			"property": "location",
			"title": "Events.Places.TitleSingular"
		},
		{
			"name": "description",
			"property": "description",
			"title": "Events.Description",
			"label": "Events.Description",
			"hidden": false
		},
		{
			"name": "courseClass/title",
			"property": "courseClass",
			"title": "Events.Classes.Title",
			"formatter": "TemplateFormatter",
			"formatString": "${courseClassDisplayCode} - ${courseClass}"
		},
		{
			"name": "courseClass/course/displayCode",
			"property": "courseClassDisplayCode",
			"title": "Events.Classes.DisplayCode",
			"hidden": true
		},
		{
			"name": "sections",
			"property": "sections",
			"title": "Events.ClassSections",
			"label": "Events.ClassSections",
			"hidden": false,
			"sortable": false,
			"virtual": true,
			"formatters": [
				{
					"formatter": "TemplateFormatter",
					"formatString": "${sections && Array.isArray(sections) && sections.length > 0 ?'<span class=\"text-center\">' + sections.map(x=> x.name).join(',') + '</span>':'-'}"
				}
			]
		},
		{
			"name": "performer/alternateName",
			"property": "performer",
			"title": "Events.Performer",
			"label": "Events.Performer"
		},
		{
			"name": "eventStatus/name",
			"property": "eventStatus",
			"formatter": "StatusFormatter",
			"title": "Events.StatusGeneral",
			"label": "Status",
			"hidden": false
		},
		{
			"name": "id",
			"virtual": true,
			"formatter": "ButtonFormatter",
			"className": "text-center",
			"sortable": false,
			"formatOptions": {
				"buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
				"buttonClass": "btn btn-default",
				"commands": [
					"../",
					"${id}"
				]
			}
		},
		{
			"name": "id",
			"virtual": true,
			"sortable": false,
			"formatString": "(modal:delete)",
			"className": "text-center",
			"formatters": [
				{
					"formatter": "ButtonFormatter",
					"formatOptions": {
						"buttonContent": "<i class=\"fa fa-trash text-indigo\" aria-hidden=\"true\"></i>",
						"buttonClass": "btn btn-default",
						"commands": [
							"${id}",
							"delete"
						],
						"navigationExtras": {
							"replaceUrl": false,
							"skipLocationChange": true
						}
					}
				},
				{
					"formatter": "NgClassFormatter",
					"formatOptions": {
						"ngClass": {
							"d-none": "${eventStatus === 'EventCompleted'}"
						}
					}
				}
			]
		},
		{
			"name": "id",
			"virtual": true,
			"sortable": false,
			"className": "text-center",
			"formatters": [
				{
					"formatter": "ButtonFormatter",
					"formatOptions": {
						"buttonContent": "<i class=\"fa fa-edit text-indigo\" aria-hidden=\"true\"></i>",
						"buttonClass": "btn btn-default",
						"commands": [
							{
								"outlets": {
									"modal": [
										"${id}",
										"edit"
									]
								}
							}
						]
					}
				},
				{
					"formatter": "NgClassFormatter",
					"formatOptions": {
						"ngClass": {
							"d-none": "${eventStatus === 'EventCompleted'}"
						}
					}
				}
			]
		}
	],
	"searches": [],
	"sources": [
		{
			"name": "Events.All",
			"tableConfigSrc": "assets/lists/TeachingEvents/all.json",
			"searchConfigSrc": "assets/lists/TeachingEvents/search.all.json",
			"calendarView": true,
			"icsExport": true
		},
		{
			"name": "Events.Recursive",
			"tableConfigSrc": "assets/lists/TeachingEvents/all.recurring.json",
			"searchConfigSrc": "assets/lists/TeachingEvents/search.all.recurring.json",
			"calendarView": false,
			"icsExport": false
		},
		{
			"name": "Events.Individual",
			"tableConfigSrc": "assets/lists/TeachingEvents/all.individual.json",
			"searchConfigSrc": "assets/lists/TeachingEvents/search.all.individual.json",
			"calendarView": true,
			"icsExport": true
		}
	],
	"criteria": [
		{
			"name": "courseClassDisplayCode",
			"filter": "(indexof(courseClass/course/displayCode, '${value}') ge 0 )",
			"type": "text"
		},
		{
			"name": "eventName",
			"filter": "(indexof(name, '${value}') ge 0)",
			"type": "text"
		},
		{
			"name": "sections",
			"filter": "(sections/section eq ${value})",
			"type": "text"
		},
		{
			"name": "minDate",
			"filter": "(startDate ge '${new Date(value).toISOString()}')",
			"type": "text"
		},
		{
			"name": "maxDate",
			"filter": "(startDate le '${new Date(new Date(value).setHours(23,59,59,59)).toISOString()}')",
			"type": "text"
		},
		{
			"name": "status",
			"filter": "(eventStatus eq '${value}')",
			"type": "text"
		},
		{
			"name": "performer",
			"filter": "(${(Array.isArray(value) ? value : [value]).map(val => 'performer/alternateName eq ' + \"'\" + val + \"'\").join(' or ')})",
			"type": "text"
		},
		{
			"name": "courseClass",
			"filter": "(${(Array.isArray(value) ? value : [value]).map(val => 'courseClass eq ' + \"'\" + val + \"'\").join(' or ')})",
			"type": "text"
		}
	]
}
